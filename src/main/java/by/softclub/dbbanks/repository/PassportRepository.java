package by.softclub.dbbanks.repository;

import by.softclub.dbbanks.domain.Passport;
import by.softclub.dbbanks.domain.PassportId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PassportRepository extends JpaRepository<Passport, PassportId> {
}
