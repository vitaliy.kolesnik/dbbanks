package by.softclub.dbbanks.repository;

import by.softclub.dbbanks.domain.Bank;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface BankRepository extends JpaRepository<Bank, Integer> {
    Set<Bank> findAllByMfoIn(Set<String> mfos);
}
