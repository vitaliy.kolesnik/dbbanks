package by.softclub.dbbanks.service;

import by.softclub.dbbanks.domain.dto.ClientDto;

import java.util.List;

public interface ClientService {
    List<ClientDto> getClientList();
    int createClient(ClientDto clientDto);
    void deleteClient(int id);
}
