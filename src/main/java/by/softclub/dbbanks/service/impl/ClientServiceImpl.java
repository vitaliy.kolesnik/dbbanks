package by.softclub.dbbanks.service.impl;

import by.softclub.dbbanks.domain.Bank;
import by.softclub.dbbanks.domain.Client;
import by.softclub.dbbanks.domain.Passport;
import by.softclub.dbbanks.domain.PassportId;
import by.softclub.dbbanks.domain.dto.ClientDto;
import by.softclub.dbbanks.repository.BankRepository;
import by.softclub.dbbanks.repository.ClientRepository;
import by.softclub.dbbanks.repository.PassportRepository;
import by.softclub.dbbanks.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private BankRepository bankRepository;

    @Autowired
    private PassportRepository passportRepository;

    @Override
    public List<ClientDto> getClientList() {
        return clientRepository.findAll().stream()
                .map(this::clientToClientDto)
                .collect(Collectors.toList());
    }

    private ClientDto clientToClientDto(Client client) {
        var dto = new ClientDto();
        dto.setName(client.getName());
        dto.setPassportSeries(client.getPassport().getPassportId().getSeries());
        dto.setPassportNumber(client.getPassport().getPassportId().getNumber());
        dto.setBanks(client.getBanks()
                .stream()
                .map(Bank::getMfo)
                .collect(Collectors.toSet()));
        return dto;
    }

    @Override
    public int createClient(ClientDto clientDto) {
        var client = new Client();
        client.setName(clientDto.getName());

        var passportId = new PassportId();
        passportId.setSeries(clientDto.getPassportSeries());
        passportId.setNumber(clientDto.getPassportNumber());
        Optional<Passport> optPassport = passportRepository.findById(passportId);
        if (optPassport.isEmpty()) {
            throw new RuntimeException("Паспорт не найден");
        }
        var passport = optPassport.get();
//        passport.setClient(client);
//        passportRepository.save(passport);
        client.setPassport(passport);

        Set<Bank> banks = bankRepository.findAllByMfoIn(clientDto.getBanks());
        client.setBanks(banks);

        clientRepository.save(client);

        return client.getId();
    }

    @Override
    public void deleteClient(int id) {
        clientRepository.deleteById(id);
    }
}
