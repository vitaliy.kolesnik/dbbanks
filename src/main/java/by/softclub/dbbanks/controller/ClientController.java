package by.softclub.dbbanks.controller;

import by.softclub.dbbanks.domain.dto.ClientDto;
import by.softclub.dbbanks.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/client")
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;

    @GetMapping
    public List<ClientDto> clientList() {
        return clientService.getClientList();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public int createClient(@RequestBody ClientDto clientDto) {
        return clientService.createClient(clientDto);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteClient(@PathVariable int id) {
        clientService.deleteClient(id);
    }

}
