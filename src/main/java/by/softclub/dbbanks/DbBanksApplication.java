package by.softclub.dbbanks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbBanksApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbBanksApplication.class, args);
    }

}
