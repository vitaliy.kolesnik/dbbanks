package by.softclub.dbbanks.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Setter
@Getter
public class PassportId implements Serializable {
    private String series;
    private String number;
}
