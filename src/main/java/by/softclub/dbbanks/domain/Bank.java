package by.softclub.dbbanks.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@EqualsAndHashCode(exclude = "clients")
public class Bank {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String mfo;

    private String name;

    @Embedded
    private CbuInfo cbuInfo;

    @ManyToMany(mappedBy = "banks")
    @JsonBackReference
    private Set<Client> clients = new HashSet<>();
}
