package by.softclub.dbbanks.domain.dto;

import by.softclub.dbbanks.domain.Passport;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class ClientDto {
    private String name;
    private String passportSeries;
    private String passportNumber;
    private Set<String> banks;
}
