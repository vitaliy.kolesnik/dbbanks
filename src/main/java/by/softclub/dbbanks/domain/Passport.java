package by.softclub.dbbanks.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@EqualsAndHashCode(exclude = "client")
public class Passport {
    @EmbeddedId
    private PassportId passportId;

    private LocalDate issueDate;

    @OneToOne
    @JoinColumn(name = "client_id",
            foreignKey = @ForeignKey(value = ConstraintMode.CONSTRAINT,
                    name = "fk_client"))
    private Client client;

}
