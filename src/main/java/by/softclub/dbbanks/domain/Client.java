package by.softclub.dbbanks.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    @OneToOne(mappedBy = "client", cascade = CascadeType.ALL)
    private Passport passport;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "CLIENT_BANK",
            joinColumns = {@JoinColumn(name = "CLIENT_ID")},
            inverseJoinColumns = {@JoinColumn(name = "BANK_ID")}
    )
    @JsonManagedReference
    private Set<Bank> banks = new HashSet<>();

}
